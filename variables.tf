variable "ec2_type" {
  description = "Defines EC2 type name"
  type        = string
  default     = "t2.micro"
}
